/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library test functions.
 *
 * @file      test.h
 * @see       test.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef TEST_MARS_UART_H
#define TEST_MARS_UART_H

#include "fifo.h"
#include "ringbuf.h"
#include "membuf.h"
#include "io.h"


#if TEST == 1
void test_fifo(void);
#elif TEST == 2
void test_ringbuf(void);
#elif TEST == 3
void test_membuf(void);
#else
  #error Selected TEST function is not defined!
#endif // TEST


#endif // TEST_MARS_UART_H

