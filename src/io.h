/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontrollers
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library - stream functions.
 *
 * @file      stream.h
 * @see       stream.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef MARS_STREAM_H
#define MARS_STREAM_H

#include <stdint.h>

#define NL_UNIX 1
#define NL_MAC  2
#define NL_WIN  3


extern int io_transmit(char, int (*)(uint8_t));
extern int io_receive(int (*)(void), int (*)(uint8_t));
extern int io_receive_byte(uint8_t (*)(void), int (*)(uint8_t));


#endif // MARS_STREAM_H
