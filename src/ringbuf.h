/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontrollers
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library - Ring Buffer.
 *
 * @file      ringbuf.h
 * @see       ringbuf.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * Links
 * -----
 *
 * - <https://www.mikrocontroller.net/articles/FIFO>
 * - <http://rn-wissen.de/wiki/index.php?title=FIFO_mit_avr-gcc>
 * - <http://rn-wissen.de/wiki/index.php?title=UART_mit_avr-gcc>
 * - <http://rn-wissen.de/wiki/index.php/TWI_Slave_mit_avr-gcc>
 */


#ifndef MARS_RINGBUF_H
#define MARS_RINGBUF_H

#include <stdint.h>


/** Ring buffer struct. */
typedef struct {
	uint8_t *pwrite;   ///< Write pointer.
	uint8_t *pread;    ///< Write pointer.
	uint8_t size;      ///< Buffer size (max. 255).
	uint8_t write2end; ///< Position of write pointer.
	uint8_t read2end;  ///< Position of write pointer.
} ringbuf_t;


/** Set write (put) position in ring buffer struct (inline).
 *
 * @param *r  Pointer to ring buffer data struct (ringbuf_t)
 * @param pos Requested position of write pointer
 */
inline void _inline_ringbuf_set_write_pos(ringbuf_t *r, uint8_t const pos)
{
	uint8_t const size = r->size;

	if (pos > size) {
		return;
	}

	r->pwrite += (int16_t) pos - (size - r->write2end);
	r->write2end = size - pos;
}


/** Set read (get) position in ring buffer data struct (inline).
 *
 * @param *r  Pointer to ring buffer struct (ringbuf_t)
 * @param pos Requested position of read pointer
 */
inline void _inline_ringbuf_set_read_pos(ringbuf_t *r, uint8_t const pos)
{
	uint8_t const size = r->size;

	if (pos > size) {
		return;
	}

	r->pread += (int16_t) pos - (size - r->read2end);
	r->read2end = size - pos;
}


/** Writes a data byte to the ring buffer (inline).
 *
 * This function is declared inline for a quicker execution.
 * This can be useful in interrupt service routines (ISR).
 *
 * By default use the generic function 'ringbuf_push()',
 * for more optimizing code compilation.
 *
 * @see ringbuf_push()
 *
 * @param  *r        Pointer to ringbuf struct
 * @param  data      data byte
 */
inline void _inline_ringbuf_put(ringbuf_t *r, const uint8_t data)
{
	uint8_t *pwrite = r->pwrite;
	uint8_t write2end = r->write2end;

	*(pwrite++) = data;

	if (--write2end <= 0) {
		write2end = r->size;
		pwrite -= write2end;
	}

	r->write2end = write2end;
	r->pwrite = pwrite;
}


/** Gets a data byte from the ring buffer (inline)
 *
 * @param *r  Pointer to ring buffer struct (ringbuf_t)
 * @return   data byte, got from the ring buffer
 */
inline uint8_t _inline_ringbuf_get(ringbuf_t *r)
{
	uint8_t *pread = r->pread;
	uint8_t read2end = r->read2end;

	const uint8_t data = *(pread++);

	if (--read2end <= 0) {
		read2end = r->size;
		pread -= read2end;
	}

	r->read2end = read2end;
	r->pread = pread;

	return data;
}


//extern void ringbuf_init(ringbuf_t*, uint8_t * const, const uint8_t); // TODO: if this works, it's better
extern void ringbuf_init(ringbuf_t*, uint8_t*, const uint8_t);
extern void ringbuf_set_write_pos(ringbuf_t*, const uint8_t);
extern void ringbuf_set_read_pos(ringbuf_t*, const uint8_t);
extern void ringbuf_put(ringbuf_t*, const uint8_t);
extern uint8_t ringbuf_get(ringbuf_t*);


#endif // MARS_RINGBUF_H

