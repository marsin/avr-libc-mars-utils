/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontrollers
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library - Memory Buffer.
 *
 * @file      membuf.c
 * @see       membuf.h
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * About
 * -----
 *
 *  It requires a byte (uint8_t) based buffer with a size of [1 .. 255].
 *
 *
 * Example
 * -------
 */


#include "membuf.h"


extern inline int _inline_membuf_set_pos(membuf_t*, uint8_t const);
extern inline int _inline_membuf_put(membuf_t*, uint8_t const);
extern inline int _inline_membuf_get(membuf_t*);


/** Init a new memory buffer.
 *
 * This function initializes the variables of the memory buffer struct.
 * It sets the read and write pointer on the given buffer
 * and sets the read, write, size data.
 *
 * @param *r      Pointer to memory buffer struct (membuf_t)
 * @param *buffer Pointer to buffer for this memory buffer (uint8_t)
 * @param size    Buffer size [1 .. 255]
 */
void membuf_init(membuf_t *r, uint8_t *buffer, uint8_t const size)
{
	r->prw = buffer;
	r->pos2end = r->size = size;
}


int membuf_set_pos(membuf_t *r, uint8_t const pos)
{
	return _inline_membuf_set_pos(r, pos);
}


int membuf_put(membuf_t *r, uint8_t const data)
{
	return _inline_membuf_put(r, data);
}


int membuf_get(membuf_t *r)
{
	return _inline_membuf_get(r);
}

