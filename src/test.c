/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library test functions.
 *
 * @file      test.c
 * @see       test.h
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef F_CPU
#  define F_CPU 16000000UL ///< myAVR-MK3 (ATmega2560)
#endif


#include <stdio.h>         // for fdevopen(), putchar(), getchar(), printf(), scanf()
#include <util/delay.h>    // requires F_CPU
#include <avr/interrupt.h>
#include <mars/mk3/leds.h>
#include <mars/mk3/lcd.h>
#include <mars/mk3/seg7.h>
#include <mars/mk3/switch.h>
#include <mars/mk3/joystick.h>
#include <mars/mk3/lcd_out.h>
#include <mars/mk3/lcd_terminal.h>
#include "test.h"


#if TEST == 1
/** FIFO test.
 *
 * The FIFO has a buffer size of 10 bytes (characters)
 * The FIFO becomes filled, in a 'for' loop,
 * which counts from 'a' to 'z' - 26 bytes, (characters).
 * When the FIFO is full, the FIFO put function returns '-1',
 * and the for loop becomes a break up.
 * The putted character becomes printed on the LCD.
 *
 * The FIFO becomes emptied by getting the bytes (characters) from the FIFO,
 * as long the get function returns positive values.
 * In the case the get function returns a negative value it signals it is empty
 * and the 'while' loop becomes left.
 */
void test_fifo(void)
{
	// Init test board
	mk3_lcd_terminal_init();
	mk3_lcd_light(1);

	// Init streaming IO functions <stdio.h>
	FILE lcd_stream = FDEV_SETUP_STREAM(mk3_lcd_terminal_putchar, NULL, _FDEV_SETUP_WRITE);
	stdin = NULL;
	stdout = stderr = &lcd_stream;

	// Init FIFO
	const uint8_t BUFFER_SIZE = 10;
	uint8_t buffer[BUFFER_SIZE];
	fifo_t fifo;
	fifo_init(&fifo, buffer, BUFFER_SIZE);

	// Fill FIFO (put)
	printf("fifo_put()\n");
	for (char c = 'a'; c <= 'z'; ++c) {
		if (fifo_put(&fifo, c) < 0) { // check for FIFO_FULL
			break;
		}
		putchar(c);
	}

	// Empty FIFO (get)
	int i = 0;
	printf("\n\nfifo_get_nowait()\n");
	while ((i = fifo_get_nowait(&fifo)) >= 0) { // check for FIFO_EMPTY
		putchar((char) i);
	}
}


#elif TEST == 2
//  #error Selected TEST function is not implemented yet!
/** Ring buffer test. */
void test_ringbuf(void)
{
	uint8_t const lcd_width = 21; // chars per line
	uint8_t const lcd_height = 8; // lines
	uint8_t const lcd_size = lcd_width * lcd_height;

	uint8_t const buffer_size = 26; // A-Z

	// init LCD
	mk3_lcd_init();
	mk3_lcd_clear();
	mk3_lcd_set_pos(0, 0);
	mk3_lcd_light(1);

	// init ring buffer
	uint8_t buffer[buffer_size];
	ringbuf_t buffer_data;

	ringbuf_init(&buffer_data, buffer, buffer_size);

	// output loop
	uint8_t toggle = 0;
	while (!0) {

		// fill buffer
		switch (toggle) {
			case 0:
				ringbuf_set_write_pos(&buffer_data, 0);
				for (uint8_t i = 0; i < buffer_size; ++i) {
					ringbuf_put(&buffer_data, i + 'a');
				}
				toggle = 1;
				break;
			case 1:
				ringbuf_set_write_pos(&buffer_data, 13);
//				ringbuf_set_write_pos(&buffer_data, 0);
				for (uint8_t i = 0; i < buffer_size; ++i) {
					ringbuf_put(&buffer_data, i + 'A');
				}
				toggle = 0;
				break;
		}

		// output
		mk3_lcd_set_pos(0, 0);
		ringbuf_set_read_pos(&buffer_data, 0);
		for (uint8_t i = 0; i < lcd_size; ++i) {
			if (i % 21 == 0) {
				if (i / 21 < 8) {
					mk3_lcd_set_pos(0, 8 * (i / 21)); // next line
				} else {
					mk3_lcd_set_pos(0, 0); // first line
				}
			}
			mk3_lcd_putchar(ringbuf_get(&buffer_data), NULL); // print data from buffer
		}

	_delay_ms(2500);
	}
}


#elif TEST == 3
void test_membuf(void)
{
	uint8_t const lcd_width = 21; // chars per line
	uint8_t const lcd_height = 8; // lines
	uint8_t const lcd_size = lcd_width * lcd_height;

	uint8_t const buffer_size = 10; // A-J
	int c = 0;

	// init LCD
	mk3_lcd_init();
	mk3_lcd_clear();
	mk3_lcd_set_pos(0, 0);
	mk3_lcd_light(1);

	// init memory buffer
	uint8_t buffer[buffer_size];
	membuf_t buffer_data;

	membuf_init(&buffer_data, buffer, buffer_size);

	// output loop
	uint8_t toggle = 0;
	while (!0) {

		// fill buffer
		switch (toggle) {
			case 0:
				membuf_set_pos(&buffer_data, 0);
				for (uint8_t i = 0; i < buffer_size; ++i) {
					membuf_put(&buffer_data, i + 'a');
				}
				toggle = 1;
				break;
			case 1:
				if (membuf_set_pos(&buffer_data, 13)) { // produces always 'wrong', buffer has just a size of 10
					membuf_set_pos(&buffer_data, 5);
					for (uint8_t i = 0; i < buffer_size; ++i) {
						membuf_put(&buffer_data, i + 'A');
					}
				} else {
					membuf_set_pos(&buffer_data, 2);
					for (uint8_t i = 0; i < buffer_size; ++i) {
						membuf_put(&buffer_data, i + '0');
					}
				}
				toggle = 0;
				break;
		}

		// output
		mk3_lcd_set_pos(0, 0);
		membuf_set_pos(&buffer_data, 0);
		for (uint8_t i = 0; i < lcd_size; ++i) {
			if (i % 21 == 0) {
				if (i / 21 < 8) {
					mk3_lcd_set_pos(0, 8 * (i / 21)); // next line
				} else {
					mk3_lcd_set_pos(0, 0); // first line
				}
			}
			// print data from buffer
			if ((c = membuf_get(&buffer_data)) >= 0) { // returns -1 if buffer pos is at the end
				mk3_lcd_putchar((char) c, NULL);
			} else {
				mk3_lcd_putchar('.', NULL);
			}
		}
		_delay_ms(2500);
	}
}

#endif // TEST

