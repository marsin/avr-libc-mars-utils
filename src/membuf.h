/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontrollers
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library - Memory Buffer.
 *
 * @file      membuf.h
 * @see       membuf.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * Links
 * -----
 *
 * - <https://www.mikrocontroller.net/articles/FIFO>
 * - <http://rn-wissen.de/wiki/index.php?title=FIFO_mit_avr-gcc>
 * - <http://rn-wissen.de/wiki/index.php?title=UART_mit_avr-gcc>
 * - <http://rn-wissen.de/wiki/index.php/TWI_Slave_mit_avr-gcc>
 */


#ifndef MARS_MEMBUF_H
#define MARS_MEMBUF_H

#include <stdint.h>


/** Memory buffer struct. */
typedef struct {
	uint8_t *prw;      ///< Read-/Write pointer.
	uint8_t size;      ///< Buffer size (max. 255).
	uint8_t pos2end;   ///< Position of read-/write pointer.
} membuf_t;


inline int _inline_membuf_set_pos(membuf_t *r, uint8_t const pos)
{
	uint8_t const size = r->size;

	if (pos > size) {
		return -1;
	}

	r->prw += (int16_t) pos - (size - r->pos2end);
	r->pos2end = size - pos;

	return (int) pos;
}


inline int _inline_membuf_put(membuf_t *r, const uint8_t data)
{
	uint8_t *prw = r->prw;
	uint8_t pos2end = r->pos2end;

	if (pos2end-- == 0) {
		return -1;
	}

	*(prw++) = data;

	r->pos2end = pos2end;
	r->prw = prw;

	return (int) data;
}


inline int _inline_membuf_get(membuf_t *r)
{
	uint8_t *prw = r->prw;
	uint8_t pos2end = r->pos2end;

	if (pos2end-- == 0) {
		return -1;
	}

	uint8_t const data = *(prw++);

	r->pos2end = pos2end;
	r->prw = prw;

	return (int) data;
}


extern void membuf_init(membuf_t*, uint8_t*, uint8_t const);
extern int membuf_set_pos(membuf_t*, uint8_t const);
extern int membuf_put(membuf_t*, uint8_t const);
extern int membuf_get(membuf_t*);


#endif // MARS_MEMBUF_H

