/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontrollers
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library - Manual FIFO.
 *
 * @file      mfifo.h
 * @see       mfifo.c
 * @see       fifo.h
 * @see       fifo.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * Links
 * -----
 *
 * - <https://www.mikrocontroller.net/articles/FIFO>
 */


#warning "These functions are not tested for integrity!"

#ifndef MARS_MFIFO_H
#define MARS_MFIFO_H

#include <stdint.h>
#include <avr/interrupt.h>


// Status codes
#define MFIFO_FULL  -1 ///< Status: Manual FIFO is full - no next write pos available
#define MFIFO_EMPTY -2 ///< Status: Manual FIFO is empty - no next read pos available
#define MFIFO_POS_INVALID -3 ///< Invalid position


/** Manual FIFO struct. */
typedef struct {
	uint8_t *pread, *pwrite;     ///< Read and write pointer.
	uint8_t size;                ///< Buffer size (max. 255).
	uint8_t read2end, write2end; ///< Position of read and write pointer.
	uint8_t volatile count;      ///< Count of buffered bytes (space between read and write pointer).
} mfifo_t;


/** Increment write pointer position.
 *
 * @retval MFIFO_FULL Manual FIFO is full (read pointer is directly ahead)
 * @retval 0          OK
 */
inline int_fast8_t _inline_mfifo_next_write_pos(mfifo_t *f)
{
	if (f->count >= f->size) {
		return MFIFO_FULL;
	}

	uint8_t *pwrite = f->pwrite;
	uint8_t write2end = f->write2end;

	++pwrite;

	if (--write2end <= 0) {
		write2end = f->size;
		pwrite -= write2end;
	}

	f->write2end = write2end;
	f->pwrite = pwrite;

	uint8_t sreg = SREG;
	cli();
	f->count++;
	SREG = sreg;

	return 0;
}


/** Increment read pointer position.
 *
 * @retval MFIFO_EMPTY Manual FIFO is empty 
 *                     (write pointer has same position as readpointer)
 * @retval 0           OK
 */
inline int_fast8_t _inline_mfifo_next_read_pos(mfifo_t *f)
{
	if (f->count == 0) {
		return MFIFO_EMPTY;
	}

	uint8_t *pread = f->pread;
	uint8_t read2end = f->read2end;
	++pread;

	if (--read2end == 0) {
		read2end = f->size;
		pread -= read2end;
	}

	f->pread = pread;
	f->read2end = read2end;

	uint8_t sreg = SREG;
	cli();
	f->count--;
	SREG = sreg;

	return 0;
}


/** Set write (put) position in ring buffer struct (inline).
 *
 * @param *r  Pointer to ring buffer data struct (ringbuf_t)
 * @param pos Requested position of write pointer
 */
inline int _inline_mfifo_set_write_pos(mfifo_t *f, uint8_t const pos)
{
	uint8_t const size = f->size;
	uint8_t const read2end = f->read2end;
	uint8_t write2end = f->write2end;

	if (pos > size) {
		return MFIFO_POS_INVALID;
	}

	f->pwrite += (int_fast16_t) pos - (size - write2end);
	f->write2end = write2end = size - pos;

	if (write2end >= read2end) {
		f->count = write2end - read2end;
	} else {
		f->count = (size - read2end) + write2end; 
	}

	return (int) pos;
}


/** Set read (get) position in ring buffer data struct (inline).
 *
 * @param *r  Pointer to ring buffer struct (ringbuf_t)
 * @param pos Requested position of read pointer
 */
inline int _inline_mfifo_set_read_pos(mfifo_t *f, uint8_t const pos)
{
	uint8_t const size = f->size;
	uint8_t const write2end = f->write2end;
	uint8_t read2end = f->read2end;

	if (pos > size) {
		return MFIFO_POS_INVALID;
	}

	f->pread += (int_fast16_t) pos - (size - read2end);
	f->read2end = read2end = size - pos;

	if (write2end >= read2end) {
		f->count = write2end - read2end;
	} else {
		f->count = (size - read2end) + write2end; 
	}

	return (int) pos;
}


/** Writes the data to the actual position of the manual FIFO.
 * 
 * Writes the data, but does not increment the write position automatically.
 *
 * @return given data byte (echo)
 */
inline uint8_t _inline_mfifo_write_data(mfifo_t *f, const uint8_t data)
{
	*(f->pwrite) = data;
	return data;
}


/** Reads the data from the actual position of the manual FIFO.
 *
 * Reads the data, but does not increment the read position automatically.
 *
 * @return data byte
 */
inline uint8_t _inline_mfifo_read_data(mfifo_t *f)
{
	return *(f->pread);
}


/** Puts a data byte to the Manual FIFO buffer (inline).
 *
 * This function is declared inline for a quicker execution.
 * This can be useful in interrupt service routines (ISR).
 *
 * @see mfifo_put()
 *
 * @param  *f   Pointer to Manual FIFO struct
 * @param  data data byte
 * @return      Data byte, which was stored in the buffer, type casted to int.
 */
inline int _inline_mfifo_put(mfifo_t *f, const uint8_t data)
{
	int_fast8_t ret = _inline_mfifo_next_write_pos(f);
	if (ret < 0) {
		return (int) ret;
	} else {
		return (int) _inline_mfifo_write_data(f, data);
	}
}


/** Gets a data byte from the Manual FIFO buffer (inline)
 *
 * This function is declared inline for a quicker execution.
 * This can be useful in interrupt service routines (ISR).
 * This functions does **not** check if the Manual FIFO contains readable data bytes.
 *
 * By default use the generic function 'mfifo_get_wait()'
 * and 'mfifo_get_nowait()', for more optimizing code compilation.
 *
 * @see mfifo_get_wait()
 * @see mfifo_get_nowait()
 *
 * @param *f Pointer to Manual FIFO struct
 * @return   data byte, got from the Manual FIFO
 */
inline int _inline_mfifo_get(mfifo_t *f)
{
	int_fast8_t ret = _inline_mfifo_next_read_pos(f);
	if (ret < 0) {
		return (int) ret;
	} else {
		return (int) _inline_mfifo_read_data(f);
	}
}


extern void mfifo_init(mfifo_t*, uint8_t*, const uint8_t);

extern int_fast8_t mfifo_next_write_pos(mfifo_t*);
extern int_fast8_t mfifo_next_read_pos(mfifo_t*);

extern int mfifo_set_write_pos(mfifo_t*, uint8_t const);
extern int mfifo_set_read_pos(mfifo_t*, uint8_t const);

extern int mfifo_put(mfifo_t*, const uint8_t);
extern int mfifo_get(mfifo_t*);
extern int mfifo_get_wait(mfifo_t*);


#endif // MARS_MFIFO_H

