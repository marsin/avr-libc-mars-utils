/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontrollers
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library - Ring Buffer.
 *
 * @file      ringbuf.c
 * @see       ringbuf.h
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * About
 * -----
 *
 *  It requires a byte (uint8_t) based buffer with a size of [1 .. 255].
 *
 *
 * Example
 * -------
 */


#include "ringbuf.h"


extern inline void _inline_ringbuf_set_write_pos(ringbuf_t*, uint8_t const);
extern inline void _inline_ringbuf_set_read_pos(ringbuf_t*, uint8_t const);
extern inline void _inline_ringbuf_put(ringbuf_t*, uint8_t const);
extern inline uint8_t _inline_ringbuf_get(ringbuf_t*);


/** Init a new ring buffer.
 *
 * This function initializes the variables of the ring buffer struct.
 * It sets the read and write pointer on the given buffer
 * and sets the read, write, size data.
 *
 * @param *r      Pointer to ring buffer struct (ringbuf_t)
 * @param *buffer Pointer to buffer for this ring buffer (uint8_t)
 * @param size    Buffer size [1 .. 255]
 */
//void ringbuf_init(ringbuf_t *r, uint8_t * const buffer, uint8_t const size) // TODO: if this works, it's better
void ringbuf_init(ringbuf_t *r, uint8_t *buffer, uint8_t const size)
{
	r->pwrite = r->pread = buffer;
	r->write2end = r->read2end = r->size = size;
//	r->write2end = r->read2end = r->size = sizeof(buffer);
}


/** Set write position in ring buffer struct.
 *
 * @param *r  Pointer to ring the buffer data struct (ringbuf_t)
 * @param pos Requested position of write pointer
 */
void ringbuf_set_write_pos(ringbuf_t *r, uint8_t const pos)
{
	_inline_ringbuf_set_write_pos(r, pos);
}


/** Set read position in ring buffer struct.
 *
 * @param *r  Pointer to ring buffer data struct (ringbuf_t)
 * @param pos Requested position of read pointer
 */
void ringbuf_set_read_pos(ringbuf_t *r, uint8_t const pos)
{
	_inline_ringbuf_set_read_pos(r, pos);
}


/** Writes a data byte to the ring buffer buffer.
 *
 * @see _inline_ringbuf_push()
 *
 * @param *r   Pointer to ring buffer struct
 * @param data data byte
 */
void ringbuf_put(ringbuf_t *r, uint8_t const data)
{
	_inline_ringbuf_put(r, data);
}


/** Gets a data byte from the ring buffer
 *
 * @param *r  Pointer to ring buffer struct (ringbuf_t)
 * @return   data byte, got from the ring buffer
 */
uint8_t ringbuf_get(ringbuf_t *r)
{
	return _inline_ringbuf_get(r);
}

