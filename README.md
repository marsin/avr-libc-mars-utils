Atmel AVR utils C library
=========================

About
=====

<https://gitlab.com/marsin/avr-libc-mars-utils>

Copyright (c) 2017 Martin Singer <martin.singer@web.de>


Disclaimer
----------

- This is a private project.
- It's under heavy development.
- Don't use it for productive developments.
- It comes WITHOUT ANY WARRANTY!


License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


About the library
-----------------

- This project implements assorted utility functions.
- It has some test functions, selected by the compiler directive `TEST`,
  declared in the Makefile or by the `make` command.
- The Makefile is able to install the library functions on the local system.
- The test functions are not required for compiling and installing the library.
- For installing the library for multiple Instruction Set Architectures (ISA)
  edit and use the script `install-libs.sh` (edit the Makefile as well).
- For implementing this project, a myAVR-MK3 development board was/is used,
  but the library is designed for general usage on multiple AVR ISA.


### Contained libraries

- FIFO buffer
- Ring buffer
- IO


### Supported ISA

- avr4
- avr5
- avr6


Requirements
============

General
-------

- avr-gcc (C99)
- avrdude


Libraries
---------

The MK3 library is required for the test functions,
but not for compiling and installing this UART library.

<https://gitlab.com/marsin/avr-libc-mars-mk3>


Hardware
--------

myAVR MK3 board (only required for the test functions)

- en: <http://shop.myavr.com/?sp=article.sp.php&artID=100063>
- de: <http://shop.myavr.de/?sp=article.sp.php&artID=100063>


Documentation
=============

Creating the Doxygen reference:

	$ doxygen doxygen.conf
	$ firefox doc/html/index.html

- For examples and references have a look at the files `test.h` and `test.c`
- There is usage documentation in the library source files.


Usage
=====

- `make all`        Make software.
- `make clean`      Clean out built project files.
- `make program`    Download the hex file to the device, using avrdude.
- `make install`    Install drivers as library and headers on system.
- `make uninstall`  Uninstall driver library and headers from system.


Installing the library
----------------------

Install drivers as library and headers for multiple ISA on system:

	# sh install-libs.sh


- The library becomes installed under
  - `/usr/local/avr/lib/avr4/mars/libutils.a`
  - `/usr/local/avr/lib/avr5/mars/libutils.a`
  - `/usr/local/avr/lib/avr6/mars/libutils.a`
- The headers become installed under
  - `/usr/local/avr/include/mars/utils/fifo.h`
  - `/usr/local/avr/include/mars/utils/iostream.h`
- Include the headers with
  - `#include <mars/utils/fifo.h>`
  - `#include <mars/utils/iostream.h>`
- Link the library with *(depending on your ISA)*
  - `-luart -L/usr/local/avr/lib/avr4/mars`
  - `-luart -L/usr/local/avr/lib/avr5/mars`
  - `-luart -L/usr/local/avr/lib/avr6/mars`


Programming the MC (MK3 board)
------------------------------

Select a test function with the compiler directive TEST,
defined in the Makefile or by the make command.

	$ make clean && make TEST=1 && make install

