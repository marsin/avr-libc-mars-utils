/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontrollers
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library - stream functions.
 *
 * @file      stream.c
 * @see       stream.h
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#include <stdio.h> // for EOF
#include "io.h"


/** */
int io_transmit(char c, int (*fp_transmit)(uint8_t))
{
	if (fp_transmit == NULL) {
		return EOF;
	}

	if (c == '\n') {
		io_transmit('\r', fp_transmit);
	}
	return fp_transmit((uint8_t) c);
}


/** */
int io_receive(int (*fp_receive)(void), int (*fp_transmit)(uint8_t))
{
	if (fp_receive == NULL) {
		return EOF;
	}

	int i = fp_receive();
	if (i < 0) {
		return i;
	}

	char c = (char) i;
	if (c == '\r') {
		c = '\n';
	}

	if (fp_transmit == NULL) {
		return (int) c;
	} else {
		return io_transmit(c, fp_transmit);
	}
}


/** */
int io_receive_byte(uint8_t (*fp_receive)(void), int (*fp_transmit)(uint8_t))
{
	if (fp_receive == NULL) {
		return EOF;
	}

	char c = fp_receive();
	if (c == '\r') {
		c = '\n';
	}

	if (fp_transmit == NULL) {
		return c;
	} else {
		return io_transmit(c, fp_transmit);
	}
}

