/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontrollers
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library - FIFO.
 *
 * @file      fifo.h
 * @see       fifo.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * Links
 * -----
 *
 * - <http://rn-wissen.de/wiki/index.php?title=FIFO_mit_avr-gcc>.
 * - <https://www.mikrocontroller.net/articles/FIFO>
 */


#ifndef MARS_FIFO_H
#define MARS_FIFO_H

#include <stdint.h>
#include <avr/interrupt.h>


// Status codes
#define FIFO_FULL  -1 ///< Status: FIFO is full
#define FIFO_EMPTY -2 ///< Status: FIFO is empty


/** FIFO struct. */
typedef struct {
	uint8_t *pread, *pwrite;     ///< Read and write pointer.
	uint8_t size;                ///< Buffer size (max. 255).
	uint8_t read2end, write2end; ///< Position of read and write pointer.
	uint8_t volatile count;      ///< Count of buffered bytes (space between read and write pointer).
} fifo_t;


/** Puts a data byte to the FIFO buffer (inline).
 *
 * This function is declared inline for a quicker execution.
 * This can be useful in interrupt service routines (ISR).
 *
 * By default use the generic function 'fifo_put()',
 * for more optimizing code compilation.
 *
 * @see fifo_put()
 *
 * @param  *f        Pointer to FIFO struct
 * @param  data      data byte
 * @retval FIFO_FULL Buffer is full, data byte was not stored
 * @return           Data byte, which was stored in the buffer, type casted to int.
 */
inline int _inline_fifo_put(fifo_t *f, const uint8_t data)
{
	if (f->count >= f->size) {
		return FIFO_FULL;
	}

	uint8_t *pwrite = f->pwrite;
	uint8_t write2end = f->write2end;

	*(pwrite++) = data;

	if (--write2end <= 0) {
		write2end = f->size;
		pwrite -= write2end;
	}

	f->write2end = write2end;
	f->pwrite = pwrite;

	uint8_t sreg = SREG;
	cli();
	f->count++;
	SREG = sreg;

	return (int) data;
}


/** Gets a data byte from the FIFO buffer (inline).
 *
 * This function is declared inline for a quicker execution.
 * This can be useful in interrupt service routines (ISR).
 * If there is no data in the FIFO it returns a negative value
 * to indicate there is no data in the FIFO.
 *
 * By default use the generic function 'fifo_get()'
 * and 'fifo_get_wait()', for more optimizing code compilation.
 *
 * @see fifo_get()
 * @see fifo_get_wait()
 *
 * @param *f          Pointer to FIFO struct
 * @return            Data byte, got from the FIFO
 * @retval FIFO_EMPTY Buffer is empty
 */
inline int _inline_fifo_get(fifo_t *f)
{
	if (f->count == 0) {
		return FIFO_EMPTY;
	}

	uint8_t *pread = f->pread;
	uint8_t read2end = f->read2end;
	uint8_t data = *(pread++);

	if (--read2end == 0) {
		read2end = f->size;
		pread -= read2end;
	}

	f->pread = pread;
	f->read2end = read2end;

	uint8_t sreg = SREG;
	cli();
	f->count--;
	SREG = sreg;

	return (int) data;
}


extern void fifo_init(fifo_t*, uint8_t*, const uint8_t);
extern int fifo_put(fifo_t*, const uint8_t);
extern int fifo_get(fifo_t*);
extern int fifo_get_wait(fifo_t*);


#endif // MARS_FIFO_H

