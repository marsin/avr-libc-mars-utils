ToDo
====

FIFO
----

- using `sizeof()` for calculating size of buffer array
  --> no manual size value required


Ring buffer
-----------

- using `sizeof()` for calculating size of buffer array
  --> no manual size value required


IO stream
---------

- Using pointer to the output functions					DONE
- Offer CR (carriage return) for different OS'
  - UNIX:    /n								fixed coded
  - MAC:     /r
  - Windows: /r/n
- Check if inline code (fifo.{c,h}) works right in a library

