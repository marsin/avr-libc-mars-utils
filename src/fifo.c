/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontrollers
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library - FIFO.
 *
 * @file      fifo.c
 * @see       fifo.h
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * About
 * -----
 *
 * This is a slim and quick FIFO for byte data.
 *
 * The inline functions can be useful for quicker ISR.
 * In general use the generic functions for better optimized binaries.
 *
 * The functions are (shold be) interrupt save for equivalent prioritized interrupts.
 *
 * The implementation bases on an FIFO example on
 *   <http://rn-wissen.de/wiki/index.php?title=FIFO_mit_avr-gcc>.
 *
 *
 * Usage
 * -----
 *
 * It requires a byte (uint8_t) based buffer with a size of [1 .. 255].
 *
 *
 * Example
 * -------
 *
 *	// Init FIFO
 *	const uint8_t BUFFER_SIZE = 10;
 *
 *	uint8_t buffer[BUFFER_SIZE];
 *	fifo_t fifo;
 *
 *	fifo_init(&fifo, buffer, BUFFER_SIZE);
 *
 *	// Fill FIFO
 *	for (char c = 'a'; c <= 'z'; ++c) {
 *		if (fifo_put(&fifo, c) < 0) { // check for FIFO_FULL
 *			break;
 *		}
 *		putchar((int) c);
 *	}
 *
 *	// Empty FIFO
 *	int i = 0;
 *	while ((i = fifo_get_nowait(&fifo)) >= 0) { // check for FIFO_EMPTY
 *		putchar(i);
 *	}
 */


#include "fifo.h"


extern inline int _inline_fifo_put(fifo_t*, const uint8_t);
extern inline int _inline_fifo_get(fifo_t*);


/** Init a new FIFO.
 *
 * This function initializes the variables of the FIFO struct.
 * It sets the read and write pointer on the given buffer
 * and sets the read, write, size data.
 *
 * @param *f      Pointer to FIFO struct (fifo_t)
 * @param *buffer Pointer to buffer for this FIFO (uint8_t)
 * @param size    Buffer size [1 .. 255]
 */
void fifo_init(fifo_t *f, uint8_t *buffer, const uint8_t size)
{
	f->pread = f->pwrite = buffer;
	f->read2end = f->write2end = f->size = size;
	f->count = 0;
}


/** Wrapper for `_inline_fifo_put()`.
 *
 * @see _inline_fifo_put()
 */
int fifo_put(fifo_t *f, const uint8_t data)
{
	return _inline_fifo_put(f, data);
}


/** Wrapper for `_inline_fifo_get()`.
 *
 * @see _inline_fifo_get()
 */
int fifo_get(fifo_t *f)
{
	return (int) _inline_fifo_get(f);
}


/** Gets a data byte from the FIFO buffer - waits for data if FIFO is empty.
 *
 * This function gets and returns data from the FIFO.
 * If there is no data in the FIFO it waits in a loop, till there is some.
 *
 * @see _inline_fifo_get()
 *
 * @param  *f Pointer to FIFO struct
 * @return    Data byte, got from the FIFO
 */
int fifo_get_wait(fifo_t *f)
{
	while (f->count == 0);
	return _inline_fifo_get(f);
}

