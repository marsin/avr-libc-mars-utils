/******************************************************************************
 Utils library - implemented in C for Atmel AVR microcontrollers
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Utils library - Manaual FIFO.
 *
 * @file      mfifo.c
 * @see       mfifo.h
 * @see       fifo.h
 * @see       fifo.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * About
 * -----
 *
 * This is a Manual FIFO for data bytes.
 * A manual incrementing or setting of the read and write pointer is required
 * (get() and put() do not increment the read and write position automatically).
 *
 * This can be useful if the read and write position is only to be changed if,
 * for example, the receipt of the data has been confirmed.
 * Then, if necessary, the data can be read and sent again.
 *
 * There is no status and checkup for a empty or full FIFO.
 *
 * For a slimmer and quicker FIFO use <mars/utils/fifo.h>.
 *
 *
 * Usage
 * -----
 *
 * It requires a byte (uint8_t) based buffer with a size of [1 .. 255].
 */


#warning "These functions are not tested integrity!"

#include "mfifo.h"


extern inline int_fast8_t _inline_mfifo_next_write_pos(mfifo_t*);
extern inline int_fast8_t _inline_mfifo_next_read_pos(mfifo_t*);

extern inline int _inline_mfifo_set_read_pos(mfifo_t*, uint8_t const);
extern inline int _inline_mfifo_set_write_pos(mfifo_t*, uint8_t const);

extern inline uint8_t _inline_mfifo_write_data(mfifo_t*, const uint8_t);
extern inline uint8_t _inline_mfifo_read_data(mfifo_t*);

extern inline int _inline_mfifo_put(mfifo_t*, const uint8_t);
extern inline int _inline_mfifo_get(mfifo_t*);


/** Init a new Manual FIFO.
 *
 * This function initializes the variables of the Manual FIFO struct.
 * It sets the read and write pointer on the given buffer
 * and sets the read, write, size data.
 *
 * @param *f      Pointer to Manual FIFO struct (mfifo_t)
 * @param *buffer Pointer to buffer for this Manual FIFO (uint8_t)
 * @param size    Buffer size [1 .. 255]
 */
void mfifo_init(mfifo_t *f, uint8_t *buffer, const uint8_t size)
{
	f->pread = f->pwrite = buffer;
	f->read2end = f->write2end = f->size = size;
	f->count = 0;
}


/** Wrapper function for `_inline_mfifo_next_write_pos()`.
 *
 * @see _inline_mfifo_next_write_pos()
 */
int_fast8_t mfifo_next_write_pos(mfifo_t *f)
{
	return _inline_mfifo_next_write_pos(f);
}


/** Wrapper function for `_inline_mfifo_next_read_pos()`.
 *
 * @see _inline_mfifo_next_read_pos()
 */
int_fast8_t mfifo_next_read_pos(mfifo_t *f)
{
	return _inline_mfifo_next_read_pos(f);
}


/** Wrapper function for `_inline_mfifo_set_write_pos()`.
 *
 * @see _inline_mfifo_set_write_pos()
 */
int mfifo_set_write_pos(mfifo_t *f, uint8_t const pos)
{
	return _inline_mfifo_set_write_pos(f, pos);
}


/** Wrapper function for `_inline_mfifo_set_read_pos()`.
 *
 * @see _inline_mfifo_set_read_pos()
 */
int mfifo_set_read_pos(mfifo_t *f, uint8_t const pos)
{
	return _inline_mfifo_set_read_pos(f, pos);
}


/** Wrapper for `_inline_mfifo_put()`.
 *
 * @see _inline_mfifo_put()
 */
int mfifo_put(mfifo_t *f, const uint8_t data)
{
	return _inline_mfifo_put(f, data);
}


/** Wrapper for `_inline_mfifo_get()`.
 *
 * @see _inline_mfifo_get()
 */
int mfifo_get(mfifo_t *f)
{
	return _inline_mfifo_get(f);
}


/** Gets a data byte from the Manual FIFO buffer - waits for data if Manual FIFO is empty.
 *
 * Like `mfifo_get()`, a wrapper for `_inline_mfifo_get()`,
 * but waits (polls) for data if the Manual FIFO is empty.
 *
 * @see _inline_mfifo_get()
 * @see mfifo_get()
 *
 * @param  *f Pointer to Manual FIFO struct
 * @return    Data byte, got from the Manual FIFO
 */
int mfifo_get_wait(mfifo_t *f)
{
	while (f->count == 0);
	return _inline_mfifo_get(f);
}

