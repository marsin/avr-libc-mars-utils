#!/bin/sh

mcu="atmega8 atmega32u4 atmega2560"

for i in $mcu; do
	echo "installing libs for '$i' ..."
	make clean && sudo make uninstall MCU=$i && sudo make install MCU=$i
done

